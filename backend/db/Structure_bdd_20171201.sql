DROP TABLE IF EXISTS public.users;
CREATE TABLE users (
    id serial,
    username text,
    firstname text,
    lastname text,
    email text,
    password text,
    lang text,
    role integer
);
