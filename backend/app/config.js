module.exports = {
  db: {
    host: process.env.PROD_DB_HOST ||'database',
    database: process.env.PROD_DB_DATABASE_PORTAL ||'portal',
    user: process.env.PROD_DB_USER ||'dbuser',
    password: process.env.PROD_DB_PASS ||'dbpass',
    port: process.env.DB_PORT || 5432,
  },
  redis: {
    host: process.env.REDIS_HOST || "redis",
    port: parseInt(process.env.REDIS_PORT) || 6379,
    password: process.env.REDIS_PASSWORD || ""
  },
  cache: {
    resetPasswordExpiration: parseInt(process.env.CACHE_RESET_PASSWORD_EXPIRATION)  || 60 * 60
  },
  server: {
    port: parseInt(process.env.SERVER_PORT) || 6301,
    host: process.env.HOST || "0.0.0.0",
    env: process.env.NODE_ENV || "development",
    frontEndDomain: process.env.FRONT_END_DOMAIN || "portal-local.constellation.fr"
  },
  accessTokenJwt: {
    secret: process.env.ACCESS_TOKEN_JWT_SECRET || "QxzXyKhrDqwdqwdwefqNgUpu4GDdf4BfWHrzC5B8mvHQyv8xB4UmQ4WBN9fcgtvKcZ",
    options: {
      expiresIn: process.env.ACCESS_TOKEN_JWT_EXPIRATION || 60 * 60 * 12
    }
  },
  email: {
    from: process.env.EMAIL_FROM || "noreply@localhost.fr"
  },
  log: {
    level:  process.env.LOG_LEVEL || 'debug'
  },
  upload: {
    location: '/src/app/app/uploads'
  }
};