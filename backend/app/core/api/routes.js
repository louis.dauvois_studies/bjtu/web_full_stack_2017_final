const passport = require('passport');

module.exports = function (app) {

  // Default
  app.get('/',
    require('./default/controller/default.get')
  );

  app.post('/user/login',
    require('./user/controller/user.login.js')
  );

};