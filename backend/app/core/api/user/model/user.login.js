module.exports = login;

const Promise = require('bluebird');
//const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const uuid = require('uuid');
const db_portal = require(__base + 'core/service/db_portal.js');
const config = require(__base + 'config.js');
const request = require('../const/request.js');

const logger = require('tracer').colorConsole({level: process.env.LOG_LEVEL || 'debug'});

// !TODO re hash le password
function login(body) {
  return db_portal.any(request.clearPasswordAttempt, body)
    .catch((err) => {
      logger.log(err);
      return Promise.reject(new Error('DEFAULT'))
    })
    .then((user) => {
      if (!user || user == null || user === "") {
        return Promise.reject(new Error('LOGIN_INCORRECT_CREDENTIALS'))
      }

      const accessTokenPayload = {
        id: user.id,
        email: user.email,
        username: user.username,
        password: user.password,
        jwtid: uuid.v4()
      };

      //user.remove(password);
      delete user['password'];

      const response = {
        "code": 200,
        "message": "LOGIN_SUCCESS",
        "data": user,
        token: jwt.sign(accessTokenPayload, config.accessTokenJwt.secret, config.accessTokenJwt.options)
      };

      return response;
    });
}