const User = require('../model/user.js');

const logger = require('tracer').colorConsole({level: process.env.LOG_LEVEL || 'debug'});

module.exports = function(req, res, next){

  // DO CHECK ON THE REQUEST

  if (!req.body.password || !req.body.login ){
    logger.log(req.body);
    return next(new Error('LOGIN_INVALID_REQUEST'));
  }

  // IF ALL CHECKS PASSED CALL THE MODEL TO HANNDLE THE DATABASE

  return User.login(req.body)
    .then((user) => res.json(user))
    .catch(next);
};

