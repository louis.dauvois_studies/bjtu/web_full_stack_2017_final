const saveLog = require('../api/module_security/model/moduleSecurity.saveLog');


const db_portal = require(__base + 'core/service/db_portal.js');
const config = require(__base + 'config.js');
const logger = require('tracer').colorConsole({level: process.env.LOG_LEVEL || 'debug' });
const request = require('../api/user/const/request');
module.exports = function(req, res, next){
  if (!req.get('User-Agent') || req.get('User-Agent') !== req.user.userAgent){
    // !todo SEND A MAIL TO FRANCOIS AND LOUIS IF THIS USELESS MIDDLEWARE IS CALLED
    return next(new Error('UNAUTHORIZED'))
  }
  db_portal.one(request.checkExpirePassword, {id: req.user.id})
    .catch((err) => {
      logger.debug(err);
      return next(new Error('UNAUTHORIZED'))
    })
    .then ((state) => {
      if (state.is_enable !== 'true'){
        // !todo SEND A MAIL TO FRANCOIS AND LOUIS IF THIS USELESS MIDDLEWARE IS CALLED

        return next(new Error('UNAUTHORIZED'))
      }
      if (state.is_expired === 'true'){
        // !todo SEND A MAIL TO FRANCOIS AND LOUIS IF THIS USELESS MIDDLEWARE IS CALLED
        return next(new Error('UNAUTHORIZED'))
      }
      if (state.password !== req.user.password){
        // !todo SEND A MAIL TO FRANCOIS AND LOUIS IF THIS USELESS MIDDLEWARE IS CALLED
        return next(new Error('UNAUTHORIZED'))
      }
      return next();
    });
};