const shell = require('child_process').execSync;
const multer = require('multer');
const sha1File = require('sha1-file');
const config = require(__base + 'config.js');
const logger = require('tracer').colorConsole({level: process.env.LOG_LEVEL || 'debug'});

const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const password = 'd6F3Efeq';
const fs = require('fs');
const zlib = require('zlib');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, config.upload.location)
  },
  filename: function (req, file, cb) {
    cb(null, (Math.random().toString(36) + '00000000000000000').slice(2, 10) + Date.now())
  }
});

const upload = multer({storage: storage}).single('file');

function encryptFile(file_path, savepath) {
  const r = fs.createReadStream(file_path);
  const zip = zlib.createGzip();
  const encrypt = crypto.createCipher(algorithm, password);
  const w = fs.createWriteStream(savepath);

  r.pipe(zip).pipe(encrypt).pipe(w);
}


module.exports = function (req, res, next) {
  try {
    upload(req, res, function (err) {
      if (err) {
        // An error occurred when uploading
        logger.debug(err.code);
        if (err.code === 'LIMIT_UNEXPECTED_FILE')
          return next(new Error('UPLOAD_UNEXPECTED_FILE'));
        return next(new Error('UPLOAD_UNKNOWN'));
      }
      if (!req.file || !req.file.path)
        return next(new Error('UPLOAD_MISSING_FILE'));
      sha1File(req.file.path, function (error, sum) {
        if (error) {
          logger.debug(error);
          return next(new Error('UPLOAD_CHECKSUM'));
        }
        const newPath = `${config.upload.location}/${sum}`;
        shell(`mv ${req.file.path} ${newPath}`);
        req.file.path = newPath;
        req.file.filename = sum;
        return next();
      });
    });
  } catch (err) {
    logger.debug(err);
    return next(new Error('UPLOAD_UNKNOWN'));
  }
};