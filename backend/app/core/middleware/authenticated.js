
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const config = require(__base + 'config.js');
const User = require(__base + 'core/api/user/model/user.js');
var TokenExtractor = function(req){
  var token = null;

  if ((req.headers && req.headers.authorization) || (req.query && req.query.authorization)) {
    if (req.headers.authorization)
      var parts = req.headers.authorization.split(' ');
    else if (req.query.authorization)
      var parts = req.query.authorization.split(' ');

    if (parts.length == 2) {
      var scheme = parts[0],
        credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) { //<-- replace MyBearer by your own.
        token = credentials;
      }
    }
  } else if (req.param('token')) {
    token = req.param('token');
  } else if (req.query.jwt) {
    token = req.query.jwt;
  }

  return token;
}
const opts = {
  jwtFromRequest: TokenExtractor,
  secretOrKey: config.accessTokenJwt.secret
};
// !TODO LOG QUAND CA ECHOUE
module.exports = function(passport) {
  // implement passport-jwt strategy
  passport.use('authenticated', new JwtStrategy(opts, function(jwt_payload, done) {
    done(null, jwt_payload);
  }));
};
