module.exports = {
  model: {

    'LOGIN_INVALID_REQUEST': {
      code: 422,
      message: 'LOGIN_INVALID_REQUEST'
    },


    'UNAUTHORIZED': {
      code: 401,
      message: 'UNAUTHORIZED'
    },

    'LOGIN_INCORRECT_CREDENTIALS': {
      code: 403,
      message: 'LOGIN_INCORRECT_CREDENTIALS'
    },

    'NOT_FOUND': {
      code: 404,
      message: 'NOT_FOUND'
    },


    'DEFAULT': {
      code: 500,
      message: 'SERVER_ERROR'
    },

  },
  db: {
    default: {
      code: 500,
      message: 'SERVER_ERROR'
    }
  }
};
