const config = require(__base + 'config.js');
const logger = require('tracer').colorConsole({level: process.env.LOG_LEVEL || 'debug' });

const promise = require('bluebird');

const options = {
  // Initialization Options
  promiseLib: promise
};
module.exports.one = one;
module.exports.none = none;
module.exports.any = any;

const pgp = require('pg-promise')(options);

const cn_portal = {
  host: config.db.host,
  database: config.db.database,
  user: config.db.user,
  password: config.db.password
};
const database_portal = pgp(cn_portal);

/**
 * Execute a query and returns a bluebird Promise
 * @param {*} text
 * @param {*} values
 */
function one(text, values){
  return new Promise(function(resolve, reject){
    return database_portal.one(text, values)
      .then((result) => resolve(result))
      .catch((err) => reject(err));
  });
}
function none(text, values){
  return new Promise(function(resolve, reject){
    return database_portal.none(text, values)
      .then((result) => resolve(result))
      .catch((err) => reject(err));
  });
}
function any(text, values){
  return new Promise(function(resolve, reject){
    return database_portal.any(text, values)
      .then((result) => resolve(result))
      .catch((err) => reject(err));
  });
}

function getHelper(){
  return database_portal;
}

