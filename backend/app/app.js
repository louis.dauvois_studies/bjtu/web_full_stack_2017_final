global.__base = __dirname + '/';
require('dotenv').config();

/**
 * Require dependencies
 */
const express = require('express');
const cors = require('cors');
const passport = require('passport');
const bodyParser = require('body-parser');

const config = require(process.env.NODE_ENV === "production" ? "./config.prod.js" : "./config.js");
const logger = require('tracer').colorConsole({level: config.log.level});

/**
 * Create express APP
 */
const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.enable('trust proxy');

require('./core/middleware/authenticated.js')(passport);
app.get('/test', function(req, res){

});
// load API routes
require('./core/api/routes.js')(app);

// add error middleware
app.use(require('./core/middleware/error.js'));

app.listen(config.server.port);


logger.info(`************************************************`);
logger.info(`Server listening on port : ${config.server.port}`);
logger.info(`************************************************`);

module.exports = app;