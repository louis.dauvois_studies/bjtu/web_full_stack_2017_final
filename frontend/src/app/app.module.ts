import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {RouterModule} from "@angular/router";

import {ROUTES} from "./app.routes";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "./_interceptors/jwt.interceptor";
import {ViewsModule} from "./_views/views.module";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    ViewsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
