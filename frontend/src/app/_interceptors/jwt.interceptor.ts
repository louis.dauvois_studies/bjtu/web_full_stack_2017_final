import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

/**
 * This interceptor is used to add Form id token saved in session to
 * every requests.
 *
 * If there is no Form id in session it just forward the request
 */
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  /**
   * Intercept the requets and add to req the Form-id header
   * @param {HttpRequest<any>} req
   * @param {HttpHandler} next
   * @returns {Observable<HttpEvent<any>>}
   */
  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (sessionStorage.getItem('user.auth.jwt')) {
      const authReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + sessionStorage.getItem('user.auth.jwt'))
      });
      return next.handle(authReq);
    }
    const noAuthReq = req.clone();
    return next.handle(noAuthReq);
  }
}
