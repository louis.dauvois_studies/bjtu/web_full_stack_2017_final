import {Routes} from '@angular/router';

import {DefaultComponent} from "./_views/default/default.component";

export const ROUTES: Routes = [
  // Main redirect
  { path: '', redirectTo: 'default', pathMatch: 'full' },

  { path: 'default', component: DefaultComponent },

  { path: 'users', loadChildren: './_views/users/users.module#UsersModule', data: { preload: true } },

  { path: '**',  redirectTo: '/default' }
];
