import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default/default.component';
import {NbLayoutModule, NbSidebarModule, NbSidebarService, NbThemeModule} from "@nebular/theme";

@NgModule({
  imports: [
    CommonModule,
    NbLayoutModule,
    NbSidebarModule,
    NbThemeModule.forRoot({ name: 'cosmic' })
  ],
  declarations: [DefaultComponent],
  providers: [NbSidebarService],
})
export class ViewsModule { }
