import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import {RouterModule} from '@angular/router';
import {USERS_ROUTES} from "./users.routes";


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(USERS_ROUTES),
  ],
  declarations: [ProfileComponent, LoginComponent]
})
export class UsersModule { }
