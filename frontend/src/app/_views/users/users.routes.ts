import {Routes} from "@angular/router";
import {LoggedGuard} from "../../_guards/logged.guard";
import {ProfileComponent} from "./profile/profile.component";
import {LoginComponent} from "./login/login.component";

export const USERS_ROUTES: Routes = [

  { path: '', redirectTo: 'profile', pathMatch: 'full' },

  { path: 'profile', canActivate: [LoggedGuard], component: ProfileComponent },

  { path: 'login', component: LoginComponent },

];
