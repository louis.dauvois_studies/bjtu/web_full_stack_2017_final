import { Component, OnInit } from '@angular/core';
import {MENU_ITEMS} from "./pages-menu";

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  title = 'app';
  menu = MENU_ITEMS;
  constructor() { }

  ngOnInit() {
  }

}
