import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Injectable()
export class LeaderboardService {

  url = environment.api;

  constructor(private http: HttpClient,
              private router: Router) {
  }
}
