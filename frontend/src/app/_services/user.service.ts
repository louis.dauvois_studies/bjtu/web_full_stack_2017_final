import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {UserLoginRequest} from "../_models/user/requests/user-login.request";
import {UserLoginResponse} from "../_models/user/responses/user-login.response";

@Injectable()
export class UserService {

  url = environment.api;

  constructor(private http: HttpClient) {
  }

  public login(login: string, password: string): Observable<UserLoginResponse> {
    const request: UserLoginRequest = {
      login: login,
      password: password
    };

    return this.http.post<UserLoginResponse>(this.url + '/user/login', request);
  }

  public logout() {
    sessionStorage.clear();
  }

}
