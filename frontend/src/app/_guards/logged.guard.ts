import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoggedGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if ((!sessionStorage.getItem('user.auth.jwt'))) {
      console.log('JWT not present');
      this.router.navigate(['/login']);
      return false;
    }
    console.log('JWT ok');
    return true
  }
}
