export interface UserLoginRequest {

  /**
   * Username (Mendatory)
   */
  login: string;

  /**
   * Password (Mendatory)
   */
  password: string;
}

