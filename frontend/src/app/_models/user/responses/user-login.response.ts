/**
 * Response for User login request
 *
 * This request is used to authenticate the user
 */
import {UserModel} from "../user.model";

export interface UserLoginResponse {

  /**
   * HTTP status code
   */

  code?: number;

  /**
   * HTTP message
   */

  message?: number;

  /**
   * Response data 3 value possible :
   *
   * null             => Request error
   * empty            => It is a local or unknown email
   */

  data?: UserModel;

  token: string;
}

