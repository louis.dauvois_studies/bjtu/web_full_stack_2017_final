/**
 * User interface that contain all possible value for an user
 */

export interface UserModel {
  id?: number;
  username?: string;
  firstname?: string;
  lastname?: string;
  email?: string;
  password?: string;
  lang?: string;
  role?: number;
}
